function calResult() {
  var preL = +document.getElementById("selLocation").value;
  var preU = +document.getElementById("selUser").value;
  var maxScore = +document.getElementById("inputScoreMax").value;
  var score1 = +document.getElementById("inputScore1").value;
  var score2 = +document.getElementById("inputScore2").value;
  var score3 = +document.getElementById("inputScore3").value;
  var result;
  var sum = score1 + score2 + score3 + preL + preU;
  if (score1 == 0 || score2 == 0 || score3 == 0) {
    result = "Bạn đã rớt vì có môn 0đ";
  } else if (sum > maxScore) {
    result = `Điểm của bạn là: ${sum}, chúc mừng bạn đã đậu`;
  } else {
    result = `Điểm của bạn là: ${sum}, bạn đã rớt do không đủ điểm :((`;
  }
  document.getElementById("txtResult").innerHTML = result;
}
function calElecBill() {
  const first50Kw = 500;
  const next50Kw = 650;
  const next100Kw = 850;
  const next150Kw = 1100;
  const leftKw = 1300;

  var name = document.getElementById("inputName").value;
  var kw = +document.getElementById("inputKW").value;
  var billCost;
  if (kw <= 50) {
    billCost = kw * first50Kw;
  } else if (kw <= 100) {
    billCost = (kw - 50) * first50Kw + kw * next50Kw;
  } else if (kw <= 200) {
    billCost = 50 * first50Kw + 50 * next50Kw + (kw - 100) * next100Kw;
  } else if (kw <= 350) {
    billCost =
      50 * first50Kw + 50 * next50Kw + 100 * next100Kw + (kw - 200) * next150Kw;
  } else {
    billCost =
      50 * first50Kw +
      50 * next50Kw +
      100 * next100Kw +
      150 * next150Kw +
      (kw - 350) * 1300;
  }
  document.getElementById(
    "txtElecBill"
  ).innerHTML = ` Tên khách hàng: ${name}, số tiền điện phải trả: ${billCost.toLocaleString()}  `;
}
